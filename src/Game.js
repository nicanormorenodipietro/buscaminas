import React, { Component } from 'react'
import Sector from './Sector'
import _ from 'lodash'
import { tsImportEqualsDeclaration } from '@babel/types';
class Game extends Component {
    constructor(props) {
        super(props);
        this.gameState = 'play'
        this.totalSectorsHidden = null
        this.restTotalSectorHidden = this.restTotalSectorHidden.bind(this)
        this.setGame = this.setGame.bind(this)
    }

    getGameState() {
        return this.gameState
    }

    setGame(newStatus) {
        this.gameState = newStatus
    }

    restTotalSectorHidden() {
        console.log(this.totalSectorsHidden)
        this.totalSectorsHidden--
        if (this.totalSectorsHidden <= 0)
            this.gameState = 'win'
    }


    setTable(table) {
        let tableSize = 8
        let totalMines = 8
        let minesLocation = []

        this.totalSectorsHidden = tableSize * tableSize - totalMines

        //Set mines position
        for (let i = 0; i < totalMines; i++) {
            let max = tableSize
            let min = 0
            const randRow = parseInt(min + Math.random() * max);
            const randColumn = parseInt(min + Math.random() * max);
            console.log(randColumn, randRow)
            let noRepeatPosition = true
            _.map(minesLocation, (location) => {
                return location.row === randRow && location.column === randColumn ? noRepeatPosition = false : null
            })
            noRepeatPosition === true ? minesLocation.push({ row: randRow, column: randColumn }) : i--
        }

        //load the sectors of the table
        for (let i = 0; i < tableSize; i++) {
            table[i] = new Array(tableSize)
            for (let j = 0; j < tableSize; j++) {
                let haveMineValue = false
                let minesAround = 0
                _.map(minesLocation, (position) => {
                    if (i === position.row && j === position.column) haveMineValue = true
                    if ((i + 1 === position.row && j === position.column) ||
                        (i - 1 === position.row && j === position.column) ||
                        (i === position.row && j + 1 === position.column) ||
                        (i === position.row && j - 1 === position.column) ||
                        (i + 1 === position.row && j + 1 === position.column) ||
                        (i - 1 === position.row && j - 1 === position.column) ||
                        (i + 1 === position.row && j - 1 === position.column) ||
                        (i - 1 === position.row && j + 1 === position.column)
                    ) { minesAround++ }
                })
                table[i][j] = <Sector
                    key={`sector${i}${j}`}
                    ref={`sector${i}${j}`}
                    game={() => this.getGameState()}
                    openNeigthboards={
                        [
                            i < tableSize - 1 ? () => this.refs[`sector${i + 1}${j}`].forceRevele() : null,
                            i > 0 ? () => this.refs[`sector${i - 1}${j}`].forceRevele() : null,
                            j < tableSize - 1 ? () => this.refs[`sector${i}${j + 1}`].forceRevele() : null,
                            j > 0 ? () => this.refs[`sector${i}${j - 1}`].forceRevele() : null,
                            i < tableSize - 1 && j < tableSize - 1 ? () => this.refs[`sector${i + 1}${j + 1}`].forceRevele() : null,
                            i > 0 && j > 0 ? () => this.refs[`sector${i - 1}${j - 1}`].forceRevele() : null,
                            i < tableSize - 1 && j > 0 ? () => this.refs[`sector${i + 1}${j - 1}`].forceRevele() : null,
                            i > 0 && j < tableSize - 1 ? () => this.refs[`sector${i - 1}${j + 1}`].forceRevele() : null
                        ]
                    }
                    haveMine={haveMineValue}
                    minesAround={minesAround}
                    restTotalSectorHidden={() => this.restTotalSectorHidden()}
                    endGame={() => this.setGame()}
                />
            }
        }
        return table
    }

    render() {
        let table = []
        table = this.setTable(table)
        return (
            <div className="columns-row-between align-center">
                {
                    _.map(table, (row, i) => {
                        return <div style={{ flexDirection: "column" }}>
                            {
                                _.map(row, (column, j) => {
                                    return table[i][j]
                                })
                            }
                        </div>
                    })
                }
                {this.gameState === 'win' ? <div>Ganaste!</div> : null}
                {this.gameState === 'lose' ? <div>Juego terminado</div> : null}
            </div>
        )
    }
}

export default Game
