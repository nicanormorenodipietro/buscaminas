import React, { Component } from 'react'
import _ from 'lodash'
import { tsImportEqualsDeclaration } from '@babel/types';

class Sector extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
        this.reveleSector = this.reveleSector.bind(this);
        this.forceRevele = this.forceRevele.bind(this);
        this.forceUpdate = this.forceUpdate.bind(this);
        this.status = 'hidden'
        this.haveMine = false
        this.haveFlag = false
    }

    getMine() {
        this.haveMine = this.props.haveMine
        return this.haveMine
    }

    reveleSector = () => {
        let game = this.props.game()
        if (game === 'play') {
            if (this.props.haveMine) {
                this.props.endGame('lose')
                this.status = 'reveled'
            } else {
                if (this.status === 'hidden') {
                    this.props.restTotalSectorHidden()
                    this.status = 'reveled'
                    if (this.props.openNeigthboards) {
                        _.map(this.props.openNeigthboards, (open, i) => {
                            open && open()
                        })
                    }
                }
            }

        }
        this.forceUpdate()
    }

    forceRevele = () => {
        this.haveMine = this.props.haveMine
        if (!this.props.haveMine && this.status === 'hidden') {
            this.props.restTotalSectorHidden()
            this.status = 'reveled'
        }
        this.forceUpdate()
    }

    render() {
        let className
        this.status === 'hidden' ? className = 'hidden-sector' : className = 'reveled-sector'
        return (
            <div style={{ height: 40, width: 40 }}>
                <button
                    className={className}
                    onClick={() => this.reveleSector()}
                    onDoubleClick={() => { this.haveFlag = !this.haveFlag }}
                    style={{ height: "100%", width: "100%", borderRadius: 5, borderColor: "black" }}
                >
                    {
                        this.status !== "hidden"
                            ? <div style={{ height: "100%", width: "100%" }}>
                                {
                                    this.props.haveMine === true
                                        ? "X"
                                        : this.props.minesAround && this.props.minesAround > 0 ? this.props.minesAround : null
                                }
                            </div>
                            : <div style={{ backgroundColor: "blue" }}></div>
                    }
                    {
                        this.haveFlag === true ? <div>F</div> : null
                    }
                </button>

            </div >
        )
    }
}

export default Sector