import React from 'react';
import logo from './logo.svg';
import './App.css';
import Sector from './Sector'
import _ from 'lodash'
import Game from './Game'

function App() {
  let arraySectors = new Array(16);
  for (let i = 0; i < 16; i++) {
    arraySectors[i] = new Array(16);
    for (let j = 0; j < 16; j++) {
      arraySectors[i][j] = () => { this.sector.setSector() }
    }
  }

  return (
    < div className="App" >
      <div
        style={{
          width: 30 * 16,
          height: 30 * 16,
          flex: 1,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
        }}>
        {/* {
          _.map(arraySectors, (sector, i) => {
            return _.map(sector, (sec, j) => {
              return <Sector onRef={ref => (arraySectors[i][j] = ref)} />
            })
          })
        } */}
        <Game />
      </div>

    </div >
  );
}

export default App;
